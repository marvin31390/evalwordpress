<?php
/*
Plugin Name: page admin pour la liste des entreprises
Description: un plugin qui permet de faire un CRUD sur la liste des entreprises que j'ai croisé
Author: Marvin Massot
 */

function admin_compagny_list()
{
    add_menu_page(
        __('Admin compagny list', 'my-project'), // Page Title
        __('Admin compagny list', 'my-project'), // Menu Title
        'manage_options', // Capability
        'admin compagny list', // Menu Slug
        'my_admin_page_contents', // Page Callback function
        'dashicons-image-crop', // Dashicon
        2// Menu Position
    );
}
add_action('admin_menu', 'admin_compagny_list');
?>
<?php

function my_admin_page_contents()
{
    
    
global $wpdb; // On se connecte à la base de données du site
    $annuaires = $wpdb->get_results("
    SELECT id,
    nom_entreprise,
    localisation_entreprise,
    prenom_contact,
    nom_contact,
    mail_contact
    FROM wp_annuaire;
");
// print_r($annuaire);
    ?>
        <h1>Bienvenue sur la page de modification de la liste des entreprises.</h1>
        <h2>Ici vous pouvez lire, créer, modifier et supprimer des entreprises de la liste.</h2>
        <section class='container'>
            <form method="POST" action="">
                <div class="form-row">
                    <label for="exampleInputName">Nom de l'entreprise</label>
                    <input type="text" name="name" class="form-control" id="exampleInputName" aria-describedby="namelHelp" placeholder="Entrer le nom de l'entreprise" required>
                    <label for="exampleInputLocalisation">Localisation</label>
                    <input type="text" name="localisation" class="form-control" id="exampleInputLocalisation" aria-describedby="LocalisationHelp" placeholder="Ville" required>
                    <label for="exampleInputFirstNameContact">Prénom contact</label>
                    <input type="text" name="firstNameContact" class="form-control" id="exampleInputFirstNameContact" aria-describedby="firstNameContactHelp" placeholder="Entrer le prénom du contact" required>
                    <label for="exampleInputNameContact">Nom contact</label>
                    <input type="text" name="nameContact" class="form-control" id="exampleInputNameContact" aria-describedby="nameContactHelp" placeholder="Entrer le nom du contact" required>
                    <label for="exampleInputEmail1">Adresse e-mail du contact</label>
                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
                </div>
                <button type="submit" name="ajouter" class="btn btn-success mt-3">Ajouter une entreprise</button>
            </form>
        </section>
        <section class="container">
            <div class="row">
                <div class="col-12">
                    <table class="table table-hover table-dark" data-toggle="table" data-search="true" data-pagination="true">
                        <thead>
                            <tr>
                                <th data-sortable="true" data-field="Nom entreprise">Nom entreprise</th>
                                <th data-sortable="true" data-field="Localisation">Localisation</th>
                                <th data-sortable="true" data-field="Prénom">Prénom contact</th>
                                <th data-sortable="true" data-field="Nom">Nom contact</th>
                                <th data-sortable="true" data-field="Mail">Mail contact</th>
                                <th data-sortable="true" data-field="Edition">Edition</th>
                            </tr>
                        </thead>
                    <tbody>
                        <?php
                    // On boucle sur tous les articles
                    foreach ($annuaires as $annuaire) {
                        ?>
                            <tr>
                                <td><?=$annuaire->nom_entreprise?></td>
                                <td><?=$annuaire->localisation_entreprise?></td>
                                <td><?=$annuaire->prenom_contact?></td>
                                <td><?=$annuaire->nom_contact?></td>
                                <td><?=$annuaire->mail_contact?></td>
                                <td><form method="POST" action="">
                                    <button type="button" class="btn-info" value="<?php echo $annuaire->id; ?>" data-toggle="modal" data-target="#<?php echo $annuaire->nom_entreprise;?>">Modifier</button>
                                    <button value="<?php echo $annuaire->id; ?>" type="input" name="supprimer" class="btn-danger">Supprimer</button>
                                </form></td>
                                <div class="modal fade" id="<?php echo $annuaire->nom_entreprise;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="POST" action="">

                                            <section class='container'>
                                                    <div class="form-row">
                                                        <label for="exampleInputUpdateName">Nom de l'entreprise</label>
                                                            <input type="text" name="updateName" class="form-control" id="exampleInputUpdateName" aria-describedby="namelHelp" value="<?=$annuaire->nom_entreprise?>"  required>
                                                            <label for="exampleInputUpdateLocalisation">Localisation</label>
                                                            <input type="text" name="updateLocalisation" class="form-control" id="exampleInputUpdateLocalisation" aria-describedby="LocalisationHelp" value="<?=$annuaire->localisation_entreprise?>"  required>
                                                            <label for="exampleInputUpdateFirstNameContact">Prénom contact</label>
                                                            <input type="text" name="updateFirstNameContact" class="form-control" id="exampleInputUpdateFirstNameContact" aria-describedby="firstNameContactHelp" value="<?=$annuaire->prenom_contact?>"  required>
                                                            <label for="exampleInputUpdateNameContact">Nom contact</label>
                                                            <input type="text" name="updateNameContact" class="form-control" id="exampleInputUpdateNameContact" aria-describedby="nameContactHelp" value="<?=$annuaire->nom_contact?>"  required>
                                                            <label for="exampleInputUpdateEmail1">Adresse e-mail du contact</label>
                                                            <input type="email" name="updateEmail" class="form-control" id="exampleInputUpdateEmail1" aria-describedby="emailHelp" value="<?=$annuaire->mail_contact?>"  required>
                                                    </div>
                                            </section>
                                            </div>
                                            
                                                <button type="button" class="btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" value="<?php echo $annuaire->id; ?>" name="modifier" class="btn-primary">Modifier</button>
                                            
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        
    </section>
  
<?php

@$name_entreprise = $_POST['name'];
@$localisation = $_POST['localisation'];
@$first_name_contact = $_POST['firstNameContact'];
@$name_contact = $_POST['nameContact'];
@$email_contact = $_POST['email'];
@$update_name_entreprise = $_POST['updateName'];
@$update_localisation = $_POST['updateLocalisation'];
@$update_first_name_contact = $_POST['updateFirstNameContact'];
@$update_name_contact = $_POST['updateNameContact'];
@$update_email_contact = $_POST['updateEmail'];
@$send = $_POST['ajouter'];
@$update = $_POST['modifier'];
@$delete = $_POST['supprimer'];



if(isset($send)){
    global $wpdb;
    $wpdb->insert('wp_annuaire', 
    array('nom_entreprise'=>$name_entreprise, 
        'localisation_entreprise'=>$localisation,
        'prenom_contact'=>$first_name_contact,
        'nom_contact'=>$name_contact,
        'mail_contact'=>$email_contact
));
}

if(isset($delete))
{
    global $wpdb;
    $wpdb->delete('wp_annuaire', 
    array('id' => $delete));
    echo '<script>parent.window.location.reload(true);</script>';

}
if (isset($update)) {
    $wpdb->update(
        'wp_annuaire',
        array(
            'nom_entreprise' => $update_name_entreprise,
            'localisation_entreprise' => $update_localisation,
            'prenom_contact' => $update_first_name_contact,
            'nom_contact' => $update_name_contact ,
            'mail_contact' => $update_email_contact
        ),
        array('id'=>$update));
        
    echo '<script>parent.window.location.reload(true);</script>';

    ;
}}



